<?php

namespace Drupal\group_member_mgmt\GroupMembership;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Plugin\GroupContentAccessControlHandler;

/**
 * Class GroupMembershipAccessControlHandler.
 */
class GroupMembershipAccessControlHandler extends GroupContentAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function relationAccess(GroupContentInterface $group_content, $operation, AccountInterface $account, $return_as_object = FALSE) {
    // If there is no group roles field, there's nothing for us to do.
    if (!$group_content->hasField('group_roles')) {
      return parent::relationAccess($group_content, $operation, $account, $return_as_object);
    }

    // Access to own memberships.
    $membership_account = $group_content->getEntity();
    if ($membership_account && ($membership_account->id() == $account->id())) {
      // Handle viewing own membership.
      if ($operation == 'view') {
        $result = $group_content->getGroup()->hasPermission('view own group_membership content', $account);
        return $return_as_object ? AccessResult::allowedIf($result) : $result;
      }

      // Standard membership handling for self edit and leave group.
      return parent::relationAccess($group_content, $operation, $account, $return_as_object);
    }

    // Look for access to memberships with no roles.
    if (count($group_content->group_roles->referencedEntities()) == 0) {
      $roleId = $group_content->getGroup()->getGroupType()->id() . '-member';
      $perm = GroupMembershipPermissionProvider::getPerm($operation, $roleId, 'content');
      if ($group_content->getGroup()->hasPermission($perm, $account)) {
        return $return_as_object ? AccessResult::allowed() : TRUE;
      }
    }

    // Loop through roles assigned to group content. Allow if user has access
    // via applicable permission.
    foreach ($group_content->group_roles->referencedEntities() as $group_role) {
      /** @var \Drupal\group\Entity\GroupRoleInterface $group_role */
      $perm = GroupMembershipPermissionProvider::getPerm($operation, $group_role->id(), 'content');
      if ($group_content->getGroup()->hasPermission($perm, $account)) {
        return $return_as_object ? AccessResult::allowed() : TRUE;
      }
    }

    // Fall back to standard permissions and access.
    return parent::relationAccess($group_content, $operation, $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public function relationCreateAccess(GroupInterface $group, AccountInterface $account, $return_as_object = FALSE) {
    // Allow for the member (no role) permission.
    $perm = GroupMembershipPermissionProvider::getPerm('create', $group->getGroupType()->id() . '-member', 'content');
    if ($group->hasPermission($perm, $account)) {
      return $return_as_object ? AccessResult::allowed() : TRUE;
    }

    // Fall back to standard permissions and access.
    return parent::relationCreateAccess($group, $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public function entityAccess(EntityInterface $entity, $operation, AccountInterface $account, $return_as_object = FALSE) {
    // Short circuit access check for super admin and self.
    if ($entity->id() == 1) {
      return $return_as_object ? AccessResult::neutral() : FALSE;
    }
    if ($entity->id() == $account->id()) {
      return $return_as_object ? AccessResult::neutral() : FALSE;
    }

    /** @var \Drupal\group\Entity\Storage\GroupContentStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('group_content');
    $group_contents = $storage->loadByEntity($entity);

    // Iterate over the group memberships of the entity to be accessed.
    foreach ($group_contents as $group_content) {
      // Analyze roles of membership to determine access.
      if (!$group_content->hasField('group_roles')) {
        continue;
      }

      /** @var \Drupal\group\Entity\GroupRoleInterface[] $groupRoles */
      $groupRoles = $group_content->group_roles->referencedEntities();

      // Access to member only type member (no roles).
      if (count($groupRoles) == 0) {
        $roleId = $group_content->getGroup()->getGroupType()->id() . '-member';
        $perm = GroupMembershipPermissionProvider::getPerm($operation, $roleId, 'entity');
        if ($group_content->getGroup()->hasPermission($perm, $account)) {
          return $return_as_object ? AccessResult::allowed() : TRUE;
        }
      }

      // Iterate over the group roles of the membership, if the accessing user
      // has the corresponding permission then allow access.
      foreach ($groupRoles as $groupRole) {
        $perm = GroupMembershipPermissionProvider::getPerm($operation, $groupRole->id(), 'entity');
        if ($group_content->getGroup()->hasPermission($perm, $account)) {
          return $return_as_object ? AccessResult::allowed() : TRUE;
        }
      }
    }

    // Fallback to the standard membership entity access check.
    return $this->entityAccessParent($entity, $operation, $account, $return_as_object);
  }

  /**
   * Temporary implementation of a parent class's ::entityAccess.
   *
   * This code necessary for compatibility until
   * https://www.drupal.org/project/group/issues/2949408#comment-13805975 is
   * committed. After that, this class's ::entityAccess should call the parent
   * method instead of this method and this method may be removed.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to check access.
   * @param string $operation
   *   The operation access should be checked for. Usually one of "view",
   *   "update" or "delete".
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user session for which to check access.
   * @param bool $return_as_object
   *   (optional) Defaults to FALSE.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function entityAccessParent(EntityInterface $entity, $operation, AccountInterface $account, $return_as_object = FALSE) {
    if ($entity->id() == 1) {
      return $return_as_object ? AccessResult::neutral() : FALSE;
    }

    // Bypass own user.
    if ($entity->id() == $account->id()) {
      return $return_as_object ? AccessResult::neutral() : FALSE;
    }

    /** @var \Drupal\group\Entity\Storage\GroupContentStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('group_content');
    $group_contents = $storage->loadByEntity($entity);

    foreach ($group_contents as $group_content) {
      if ($operation == 'view') {
        $access = AccessResult::allowedIf($group_content->getGroup()->hasPermission('view group_membership entity', $account));
        if ($access->isAllowed()) {
          return $return_as_object ? $access : TRUE;
        }
        continue;
      }

      // Allow if can edit any.
      $allowed = $group_content->getGroup()->hasPermission($operation . ' any group_membership entity', $account);
      // Allow if can edit own.
      if (!$allowed  && $group_content->getOwnerId() == $account->id()) {
        $allowed = $group_content->getGroup()->hasPermission($operation . ' own group_membership entity', $account);
      }
      if ($allowed) {
        return $return_as_object ? AccessResult::allowed() : TRUE;
      }
    }

    return $return_as_object ? AccessResult::neutral() : FALSE;
  }

}
