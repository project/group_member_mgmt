<?php

namespace Drupal\group_member_mgmt\GroupMembership;

use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\Plugin\GroupMembershipPermissionProvider as GroupMembershipPermissionProviderBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GroupMembershipPermissionProvider.
 */
class GroupMembershipPermissionProvider extends GroupMembershipPermissionProviderBase {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, $plugin_id, array $definition) {
    $plugin = parent::createInstance($container, $plugin_id, $definition);
    $plugin->routeMatch = $container->get('current_route_match');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPermissions() {
    $permissions = parent::buildPermissions();

    $permissions['view own group_membership content'] = [
      'title' => 'View own membership',
      'allowed for' => ['member'],
    ];

    // Not great, but we've got to obtain the group type somehow.
    if (!$groupType = $this->getGroupTypeFromRoute()) {
      return $permissions;
    }

    foreach ($this->getGroupMemberRoles($groupType) as $groupRole) {
      // Group content permissions.
      $roleId = $groupRole->id();
      $createPerm = static::getPerm('create', $roleId, 'content');
      $viewPerm = static::getPerm('view', $roleId, 'content');
      $updatePerm = static::getPerm('update', $roleId, 'content');
      $deletePerm = static::getPerm('delete', $roleId, 'content');

      $permissions[$createPerm] = [
        'title' => 'Create memberships (role %role)',
        'title_args' => ['%role' => $groupRole->label()],
      ];

      $permissions[$viewPerm] = [
        'title' => 'View individual group members (role %role)',
        'title_args' => ['%role' => $groupRole->label()],
      ];

      $permissions[$updatePerm] = [
        'title' => 'Update memberships (role %role)',
        'title_args' => ['%role' => $groupRole->label()],
      ];

      $permissions[$deletePerm] = [
        'title' => 'Remove memberships (role %role)',
        'title_args' => ['%role' => $groupRole->label()],
      ];

      // Entity permissions.
      $entityViewPerm = static::getPerm('view', $roleId, 'entity');
      $entityUpdatePerm = static::getPerm('update', $roleId, 'entity');
      $entityDeletePerm = static::getPerm('delete', $roleId, 'entity');

      $permissions[$entityViewPerm] = [
        'title' => 'Entity: View individual group members (role %role)',
        'title_args' => ['%role' => $groupRole->label()],
      ];

      $permissions[$entityUpdatePerm] = [
        'title' => 'Entity: Update members (role %role)',
        'title_args' => ['%role' => $groupRole->label()],
      ];

      $permissions[$entityDeletePerm] = [
        'title' => 'Entity: Delete members (role %role)',
        'title_args' => ['%role' => $groupRole->label()],
      ];

      // Adjustments for standard "member" only.
      if ($groupRole->id() == $groupType->id() . '-member') {
        $permissions[$createPerm]['title'] = 'Create memberships';
        $permissions[$createPerm]['description'] = 'Create memberships (no group roles) for existing users. Needed for creating memberships, use additional permissions to allow for selection of additional roles.';
        $permissions[$viewPerm]['title'] = 'View individual group members (no roles)';
        $permissions[$updatePerm]['title'] = 'Update memberships (no roles)';
        $permissions[$deletePerm]['title'] = 'Remove memberships (no roles)';
        $permissions[$entityViewPerm]['title'] = 'Entity: View individual group members (no roles)';
        $permissions[$entityUpdatePerm]['title'] = 'Entity: Update memberships (no roles)';
        $permissions[$entityDeletePerm]['title'] = 'Entity: Remove memberships (no roles)';
      }
    }

    $permissions['view group_membership content']['title'] = 'View all individual group members';

    return $permissions;
  }

  /**
   * Gets roles for members of the group.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $groupType
   *   The group type.
   *
   * @return array|\Drupal\group\Entity\GroupRoleInterface[]
   *   Group roles.
   */
  protected function getGroupMemberRoles(GroupTypeInterface $groupType) {
    $roles = $groupType->getRoles(TRUE);
    $roles = array_filter($roles, function ($role) {
      /** @var \Drupal\user\RoleInterface $role */
      return $role->get('audience') == 'member';
    });
    return $roles;
  }

  /**
   * Pulls the group type from the current route.
   *
   * @return \Drupal\group\Entity\GroupTypeInterface|null
   *   The group type, or NULL if not found.
   */
  protected function getGroupTypeFromRoute() {
    // Look for the group itself.
    if ($type = $this->routeMatch->getParameter('group_type')) {
      return $type;
    }
    // @TODO: use group type of a group found in route?
    return NULL;
  }

  /**
   * Get the permission for an operation.
   *
   * @param string $op
   *   The operation.
   * @param string $roleId
   *   The group role ID.
   * @param string $target
   *   The target of the permission. Either "entity" for the entity, or
   *   "content" group content relation.
   *
   * @return string
   *   The permission string.
   */
  public static function getPerm($op, $roleId, $target) {
    return "$op group-role:$roleId group_membership $target";
  }

}
